#!/usr/bin/env bash

if [ "$#" -ne 3 ]; then
    echo "usage sh deploy.sh <aws cli profile name> <environment> <aws default region>"
    exit 1
fi

profileName=$1
environment=$2
region=$3

function setupAWSCredentials(){
    mkdir ~/.aws
    touch ~/.aws/credentials
    echo "[$profileName]" >> ~/.aws/credentials
    echo "aws_access_key_id = ${AWS_ACCESS_KEY_ID}" >> ~/.aws/credentials
    echo "aws_secret_access_key = ${AWS_SECRET_ACCESS_KEY}" >> ~/.aws/credentials
}

function createEgnikaiProblemsS3Bucket(){
    cfn-create-or-update \
        --stack-name problems-s3-bucket-${environment} \
        --template-body file://buildAndDeploy/deploy/problems-s3-bucket.yaml \
        --parameters ParameterKey=Environment,ParameterValue=${environment} \
        --region ${region} \
        --profile ${profileName} \
        --wait || exit 1
}

setupAWSCredentials

createEgnikaiProblemsS3Bucket

npm install
node index.js $environment