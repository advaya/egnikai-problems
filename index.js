const fs  = require('fs');
const { join } = require('path');
var exec = require('child_process').exec
var recursiveReader = require('fs-readdir-recursive');

var environment = (process.argv.slice(2));
console.log("argument"+environment);

//Get Json of all problems
var files = recursiveReader(__dirname+"/Problems");
const regex = new RegExp('.json$','ig');
const jsonFiles = files.filter(file => file.match(regex) && file!='problem.json');
 
//Two program names should not be same
var fileNames = files.map(file => file.split("/").pop());

function checkIfArrayIsUnique(myArray) {
    return myArray.length === new Set(myArray).size;
}

var isDuplicatesPresent = !checkIfArrayIsUnique(fileNames);
//Print duplicates if present
if(isDuplicatesPresent){
        var uniq = fileNames
        .map((file) => {
            return {count: 1, file: file}
        })
        .reduce((a, b) => {
        a[b.file] = (a[b.file] || 0) + b.count
            return a
        }, {});

        var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)
        console.log("Found duplicate problems: "+ duplicates);
        return;
}

const isDirectory = source => fs.lstatSync(source).isDirectory();
const getDirectories = source => fs.readdirSync(source).map(name => join(source, name)).filter(isDirectory);

function checkForLanguageSpecificFiles(){
    getDirectories(__dirname+"/Problems").map(problemFolder => {
        getDirectories(problemFolder).map(language => {
            if(language.match(new RegExp("java", 'i'))){
                nonJavaFiles = recursiveReader(language).filter(file => file.match(new RegExp(".*(?<!\.java)$", 'i')));
                if(nonJavaFiles.length > 0) {
                    console.log(language + "folder contains non java files");
                    return process.exit(0);
                }
            } else if(language.match(new RegExp("csharp", 'i'))){
                nonCsharpFiles = recursiveReader(language).filter(file => file.match(new RegExp(".*(?<!\.cs)$", 'i')));
                if(nonCsharpFiles.length > 0) {
                    console.log(language+ "folder contains non csharp files");
                    return process.exit(0);
                }
            }
        });
    });
}

function checkForSrcAndTestFilesCount(){
    getDirectories(__dirname+"/Problems").map(problemFolder => {
        getDirectories(problemFolder).map(language => {
            if(language.match(new RegExp("java", 'i'))){
                javaFilesInSrc = recursiveReader(language+ "/src").filter(file => file.match(new RegExp(".java$", 'i')));
                javaFilesInTest = recursiveReader(language+ "/test").filter(file => file.match(new RegExp(".java$", 'i')));
                if(javaFilesInSrc.length == 0 || javaFilesInTest.length == 0){
                    console.log("The src and test should contain atleast one file in "+ language +" folder");
                    return process.exit(0);
                }
            } else if(language.match(new RegExp("csharp", 'i'))){
                csFilesInSrc = recursiveReader(language+ "/src").filter(file => file.match(new RegExp(".cs$", 'i')));
                csFilesInTest = recursiveReader(language+ "/test").filter(file => file.match(new RegExp(".cs$", 'i')));
                if(csFilesInSrc.length == 0 || csFilesInTest.length == 0){
                    console.log("The src and test should contain atleast one file in "+ language +" folder");
                    return process.exit(0);
                }
            }
        });
    });
}

// It should contain language specific file. For Eg:Java programs should contain only .java file. 
checkForLanguageSpecificFiles();

// Problem should contain atleast 1 src file and 1 test file.
checkForSrcAndTestFilesCount();

 // Reading json of each problem and storing in an array
var problems = {};
jsonFiles.map(file => {
    var obj = JSON.parse(fs.readFileSync(__dirname+"/Problems/"+file, 'utf8'));
    // 
    obj.languages.map( language => {
        var fileNames = obj.sources[language].src;
        fileNames =  fileNames.map( file => {
            var fileName = file + "$";
            return recursiveReader(__dirname+ "/Problems").filter(file => file.match(new RegExp(fileName,'i')))[0];
        });
        obj.sources[language].src = fileNames;

        var fileNames = obj.sources[language].test;
        fileNames =  fileNames.map( file => {
            var fileName = file + "$";
            return recursiveReader(__dirname+ "/Problems").filter(file => file.match(new RegExp(fileName,'i')))[0];
        });
        obj.sources[language].test = fileNames;
    });
    problems[obj.name.replace(" ", "")] = obj;
})

var problems = {"problems": problems};

// Remove problem.json file if it exists at problems  level
if(recursiveReader(__dirname+"/Problems/").filter(file => file==='problem.json').length > 0){
    console.log("Deleting problem.json file");
    fs.unlinkSync(__dirname+"/Problems"+"/problem.json");
}

//writing JSON of all problems to file
fs.appendFile(__dirname+"/Problems/"+'problem.json', JSON.stringify(problems), function (err) {
    if (err) throw err;
    console.log('Writing problems to problems.json');
});

// Copy Problems folder to S3
const ls = exec('aws s3 sync Problems/ s3://tw-egnikai-problems-'+environment+'/Problems', function(err ){
    if(err) throw err;
    console.log('copying problems to s3');
});

// console.log(ls);